.data
	hello: .ascii "Hello world! \n"
.text
	#we are loading the address associated with hello into $a0. 
	la $a0, hello
	#storing the instruction "4" into $v0
	li $v0, 4
	#commands the computer to preform the instruction
	syscall
	
	#the instruction "10" ends the program
	li $v0, 10
	syscall
