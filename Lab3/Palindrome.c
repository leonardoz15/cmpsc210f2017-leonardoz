//Zach Leonardo
//Lab 3
//This work is mine
#include <stdio.h>
#include <string.h>

int main() {
  char firstString[100];
  char secondString[100];
  int i, index, newIndex;

  printf("Enter a string to check: \n");
  scanf("%s", firstString);
  int length = strlen(firstString);
  index = length - 1;
  newIndex = 0;

  for(i = 0; i < length; i++) {
    secondString[newIndex] = firstString[index];
    index--;
    newIndex++;
  }
  if(strcmp(firstString, secondString) == 0) {
    printf("%s is a palindrome.\n" ,firstString);
  }
  else {
    printf("%s is not a palindrome.\n" ,firstString);
  }
  return 0;
}
