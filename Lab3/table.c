//Zach Leonardo
//Lab 3: Part 2
//This work is mine
#include <stdio.h>
#include <math.h>

int main() {
  int input;
  int i;
  float first,second,third,fourth;
  printf("Enter a integer between 1 and 50: \n");
  scanf("%d", &input);

  if(input < 51 || input > 0) {
    printf("\tx\t1/x^3\tsqrt(x)\tlog_3(x)\t1.2^x\n\t-\t-----\t-------\t--------\t-----\n");
    for(i = 1; i <= input; i = i + 1){
      first = 1/pow(i,3);
      second = sqrt(i);
      third = log(i)/log(3);
      fourth = pow(1.2,i);
      printf("\t%d\t%3.5f\t%3.5f\t%3.5f\t\t%3.5f\n",i,first,second,third,fourth);
    }
  }
  else{
    printf("Integer must be between 1 and 50!\n");
  }
  return 0;
}
