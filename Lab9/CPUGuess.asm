.data
	welcome:		.asciiz		"Hello! Please think of a number between 1 and 100"
	guess1:		.asciiz		"My first guess is: "
	low:		.asciiz		"My guess was to low! My next guess is: "
	high:		.asciiz		"My guess was to high! My next guess is: "
	end1:		.asciiz		"I got it in "
	end2:		.asciiz		" tries. Your number was "
	replay:		.asciiz		"Would you like me to guess again? (1=Yes)(0=No) "
	space:		.asciiz		" "
	newline:		.asciiz		"\n"
.text
	Main:
	li $t1, 0		#initiate counter
	li $t2, 100		#sets upper bound
	li $t3, 0		#sets lower bound
	li $s0, 2		#for later
	jal Welcome		#Procedure to print welocme messages and first guess
	
	jal Feedback		#Procedure to get user feedback
	
	jal Guess		#makes the next guess

	Welcome:
	la $a0, welcome		#introduction message
	li $v0, 4
	syscall
	
	la $a0, newline
	li $v0, 4
	syscall
	
	la $a0, guess1
	li $v0, 4
	syscall
	
	li $v0, 42		#first guess is a random int
	move $a1, $t2
	syscall
	li $v0, 1
	syscall
	move $t4, $a0		#stores guess in t4
	la $a0, space
	li $v0, 4
	syscall
	
	
	addi $t1, $t1, 1	#increment counter
	
	jr $ra
	
	Feedback:
	la $a0, space
	li $v0, 4
	syscall
	li $v0, 5
	syscall
	move $t0, $v0		#saves response in t0
	
	beq $t0, 1, toHigh
	beq $t0, -1, toLow
	beq $t0, 0, correct
	
	toHigh:
	la $a0, high
	li $v0, 4
	syscall
	move $t2, $t4		#updates upper bound
	jr $ra
	
	toLow:
	la $a0, low
	li $v0, 4
	syscall
	move $t3, $t4		#updates lower bound
	jr $ra
	
	correct:
	jal Success		#procedue to print success messages
	
	jal Replay		#procedure to ask to replay
	
	li $v0, 10
	syscall
	
	Guess:
	beq $t0, 1, Upper
	beq $t0, -1, Lower
	
	Upper:
	sub $t5, $t2, $t3	#finds difference and puts it in t5
	div $t5, $s0		#difference/ 2
	mflo $t5
	add $a0, $t5, $t3	#shift right by lower bound
	li $v0, 1
	syscall
	move $t4, $a0		#replaces with new guess
	
	addi $t1, $t1 1		#increment counter
	
	jal Feedback
	j Guess
	
	Lower:
	sub $t5, $t2, $t3	#finds the difference between upper and lower
	div $t5, $s0		#difference/ 2
	mflo $t5
	add $a0, $t5, $t3	#shift right
	li $v0, 1
	syscall
	move $t4, $a0		#replaces with new guess
	
	addi $t1, $t1, 1	#counter
	
	jal Feedback
	j Guess
	
	Success:
	la $a0, end1
	li $v0, 4
	syscall
	move $a0, $t1
	li $v0, 1
	syscall
	la $a0, end2
	li $v0, 4
	syscall
	move $a0, $t4
	li $v0, 1
	syscall
	la $a0, newline
	li $v0, 4
	syscall
	
	jr $ra
	
	Replay:
	la $a0, replay
	li $v0, 4
	syscall
	
	li $v0, 5
	syscall
	
	move $t6, $v0			
	li $t7, 1
	beq $t7, $t6, Main		#determines user input
	jr $ra
	
