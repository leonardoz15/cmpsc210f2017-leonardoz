.data
	guess:		.asciiz	"I'm thinking of a number between 1 and 100, guess?"
	yourGuess:	.asciiz	"Your guess: "
	High:		.asciiz	"Your guess was to high!"
	Low:		.asciiz "Your guess was to low!"
	End1:		.asciiz	"You got it in "
	End2:		.asciiz	" tries. My number was: "
	replay:		.asciiz	"Would you like to play again? (1=Yes)(0=No) "
	newline:		.asciiz	"\n"
.text
	Main:
	li $v0, 42		#generate a random number between 1 and 100
	la $a1, 101
	syscall
	move $t1, $a0		#move random num into t1
	
	la $a0, guess		#welcome message
	li $v0, 4
	syscall
	li $t2, 0		#initiate counter in t2
	
	jal Prompt		#calls the prompt procedure
	
	jal Input		#calls for user guess and save it in t0
	
	jal TestStart		#Processes the guess and returns
	
	jal Replay		#Processes ending/replay prompts
	
	li $v0, 10
	syscall
	
	Prompt:
	la $a0 newline
	li $v0, 4
	syscall
	
	la $a0, yourGuess
	li $v0, 4
	syscall
	
	jr $ra
	
	Input:
	li $v0, 5
	syscall
	move $t0, $v0		#move guess into t0
	jr $ra
	
	TestStart:
	move $s0, $ra		#setting aside the return address
	Test:
	bgt $t0, $t1, high
	blt $t0, $t1, low
	beq $t0, $t1, correct
	
	correct:
	addi $t2, $t2, 1	#increment counter
	move $ra, $s0		#restore correct return address
	jr $ra
	
	high:		
	la $a0, High
	li $v0, 4
	syscall
	jal Prompt
	jal Input
	addi $t2, $t2, 1
	j Test
	
	low:
	la $a0, Low
	li $v0, 4
	syscall
	jal Prompt
	jal Input
	addi $t2, $t2, 1
	j Test
	
	Replay:
	la $a0 newline
	li $v0, 4
	syscall
	
	la $a0, End1
	li $v0, 4
	syscall
	
	move $a0, $t2
	li $v0, 1
	syscall
	
	la $a0, End2
	li $v0, 4
	syscall
	
	move $a0, $t1
	li $v0, 1
	syscall
	
	la $a0 newline
	li $v0, 4
	syscall
	
	la $a0, replay
	li $v0, 4
	syscall
	
	li $v0, 5
	syscall
	
	move $t3, $v0		#response in t3
	li $t4, 1
	beq $t4, $t3, Main	#restarts
	jr $ra
	
	
	
