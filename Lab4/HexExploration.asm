.data
    hello:      .asciiz     "Hello world!\n"
    number1:    .word       42
    number2:    .half       21
    number3:    .half       1701
    number4:    .byte       73
    number5:    .half       -1701
    number6:    .byte       127
    number7:    .word       65536
    sum:        .word	   0
   difference:    .word	   0
    
    #######################
    #  ADDRESS   #  DATA  #
    #######################
    # 0x10010010 #        #
    # 0x10010011 #        #
    # 0x10010012 #        #
    # 0x10010013 #        #
    # 0x10010014 #        #1701
    # 0x10010015 #        #
    # 0x10010016 #        #
    # 0x10010017 #        #
    # 0x10010018 #        #-1701
    # 0x10010019 #        #
    # 0x1001001a #        #42
    # 0x1001001b #        #
    # 0x1001001c #        # 127
    # 0x1001001d #        #
    # 0x1001001e #        #
    # 0x1001001f #        #
    # 0x10010020 #        #65536
    # 0x10010021 #        #
    # 0x10010022 #        #
    # 0x10010023 #        #
    # 0x10010024 #        #
    # 0x10010025 #        #
    # 0x10010026 #        #
    # 0x10010027 #        #
    #######################


.text
    #Loading each value in $t0 registers
    lw $t0, number1	#42
    lh $t1, number2	#21
    lh $t2, number3	#1701
    lb $t3, number4	#73
    lh $t4, number5	#-1701
    lb $t5, number6	#127
    lw $t6, number7	#65536
    
    add $t8 $t2 $t4	#adds 1701 and -1701 together and saves the sum in $t8
    sub $t9 $t4 $t2	#subtracts -1071 and 1701 and saves the difference in $t9
    sw $t8, sum		#saves the sum
    sw $t9, difference	#saves the difference
    la $a0, hello
    li $v0, 4
    syscall
    
    li $v0, 10
    syscall
